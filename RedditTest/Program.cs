﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RedditdotnetV2;
using RedditdotnetV2.Endpoints;
using RedditdotnetV2.PushshiftAPI;
using RedditdotnetV2.PushshiftAPI.Endpoints;

namespace RedditTest
{
    public class Program
    {
        public static void Main(string[] args)
        {
            new Program().MainAsync().GetAwaiter().GetResult();
        }

        public async Task MainAsync()
        {
            RedditClient redditClient = new RedditClient();
            PushshiftClient pushshiftClient = new PushshiftClient();

            (await redditClient.DankMemes.GetPosts(RedditdotnetV2.Responses.SortType.Frontpage, 10, false)).ForEach(x =>
            {
                Console.WriteLine(x.data.url);
            });

            Console.WriteLine((await pushshiftClient.DankMemes.GetMetadata()).total_results);

            (await pushshiftClient.DankMemes.GetPosts(10, RedditdotnetV2.Responses.SortTime.Today)).ForEach(x =>
            {
                Console.WriteLine(x.full_link);
                Console.WriteLine(x.created_utc);
                Console.WriteLine();
            });

            (await pushshiftClient.DankMemes.GetPosts()).ForEach(x =>
            {
                Console.WriteLine(x.full_link);
                Console.WriteLine(x.created_utc);
                Console.WriteLine();
            });

            Console.ReadKey();
        }
    }
}
