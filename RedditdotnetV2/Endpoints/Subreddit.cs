﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

using static RedditdotnetV2.Dependencies;
using RedditdotnetV2.Responses;
using RedditdotnetV2.Helpers;

namespace RedditdotnetV2.Endpoints
{
    public class Subreddit
    {
        //Subreddit Name
        public string Name { get; set; }
        //Subreddit URL ( Format: .../r/ 'dankmemes' /... )
        public string URL { get; set; }
        public Universal Universal { get; set; }

        public Subreddit(string name, string subUrl)
        {
            Name = name;
            URL = subUrl;

            string joinedURL = BaseUrl + URL;
            Universal = new Universal(joinedURL);
            //Example: https://reddit.com/r/dankmemes/
            //         BaseUrl--------------subUrl::::
        }

        public async Task<List<Post>> GetPosts(SortType sortType, int amount, bool includeStickied)
        {
            string url = "";

            switch (sortType)
            {
                case SortType.Frontpage:
                    url += $".json?limit={amount}";
                    break;
                case SortType.Hot:
                    url += $"hot/.json?limit={amount}";
                    break;
                case SortType.Top:
                    url += $"top/.json?limit={amount}";
                    break;
                case SortType.Rising:
                    url += $"rising/.json?limit={amount}";
                    break;
                case SortType.Controversial:
                    url += $"controversial/.json?limit={amount}";
                    break;
                case SortType.New:
                    url += $"new/.json?limit={amount}";
                    break;
            }

            if (!includeStickied)
            {
                //List<Post> posts = new List<Post>();
                //(await Universal.GET<SubredditRES>(url)).data.children.ForEach(x =>
                //{
                //    if (!x.data.stickied)
                //        posts.Add(x);
                //});
                return (await Universal.GET<SubredditRES>(url)).data.children.Where(x => !x.data.stickied).ToList();
            }

            return (await Universal.GET<SubredditRES>(url)).data.children;
        }

        public async Task<List<Post>> GetPosts(SortType sortType, string search)
        {
            string url = $"search.json?q={Uri.EscapeUriString(search)}&restrict_sr=1";

            return (await Universal.GET<SubredditRES>(url)).data.children;
        }

        public async Task<List<Post>> GetPosts(SortType sortType, int pages)
        {
            string url = "";
            int amount = 100;

            switch (sortType)
            {
                case SortType.Frontpage:
                    url += $".json?limit={amount}";
                    break;
                case SortType.Hot:
                    url += $"hot/.json?limit={amount}";
                    break;
                case SortType.Top:
                    url += $"top/.json?limit={amount}";
                    break;
                case SortType.Rising:
                    url += $"rising/.json?limit={amount}";
                    break;
                case SortType.Controversial:
                    url += $"controversial/.json?limit={amount}";
                    break;
                case SortType.New:
                    url += $"new/.json?limit={amount}";
                    break;
            }

            List<Post> posts = new List<Post>(1001);
            SubredditRES subredditRES = null;

            if (pages > 10)
                pages = 10;

            for (int i = 0; i < pages; i++)
            {
                if (i == 0)
                {
                    subredditRES = await Universal.GET<SubredditRES>(url);
                    posts.AddRange(subredditRES.data.children.Where(x => !x.data.stickied).ToList());
                }
                else
                {
                    if (string.IsNullOrEmpty(subredditRES.data.after))
                        continue;

                    SubredditRES subredditRES2 = await Universal.GET<SubredditRES>($"{url}&after={subredditRES.data.after}");
                    posts.AddRange(subredditRES2.data.children.Where(x => !x.data.stickied).ToList());
                    subredditRES = subredditRES2;
                }
            }

            Console.WriteLine(posts.Count);

            return posts;
        }
    }
}
