﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

using RedditdotnetV2.Endpoints;

namespace RedditdotnetV2
{
    public class RedditClient
    {
        public List<Subreddit> Subreddits { get; set; }
        public Subreddit DankMemes { get; set; }
        public Subreddit WholesomeMemes { get; set; }
        public Subreddit Me_IRL { get; set; }

        public RedditClient()
        {
            DankMemes = new Subreddit("Dank Memes", "dankmemes/");
            WholesomeMemes = new Subreddit("Wholesome Memes", "wholesomememes/");
            Me_IRL = new Subreddit("Me_IRL", "me_irl/");
            Subreddits = new List<Subreddit>();
        }
    }
}
