﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

using RedditdotnetV2.PushshiftAPI.Responses;
using RedditdotnetV2.PushshiftAPI.Endpoints;
using RedditdotnetV2.Helpers;
using static RedditdotnetV2.Dependencies;

namespace RedditdotnetV2.PushshiftAPI.Endpoints
{
    public class SubredditMD
    {
        public string Name { get; set; }
        public string URL { get; set; }
        public Universal Universal { get; set; }

        public SubredditMD(string name, string url)
        {
            Name = name;
            URL = "?subreddit=" + url;

            Universal = new Universal(PSBaseUrl);
        }

        public async Task<Metadata> GetMetadata()
        {
            string url = URL + "&metadata=true";
            return (await Universal.GET<SubredditMDRES>(url)).metadata;
        }

        public async Task<Metadata> GetMetadata(RedditdotnetV2.Responses.SortTime sortTime)
        {
            string url = URL + $"&metadata=true&after={GetUnixTimestamp(sortTime, false)}";
            return (await Universal.GET<SubredditMDRES>(url)).metadata;
        }

        public async Task<List<Post>> GetLatestPosts()
        {
            string url = URL + "&metadata=true&size=100";
            return (await Universal.GET<SubredditMDRES>(url)).data;
        }

        public async Task<List<Post>> GetLatestPosts(int amount)
        {
            string url = URL + $"&metadata=true&size={amount}";
            return (await Universal.GET<SubredditMDRES>(url)).data;
        }

        public async Task<List<Post>> GetPosts()
        {
            Random random = new Random();
            
            string url = URL + $"&metadata=true&size=100&after={GetUnixTimestamp(Random(), false)}";
            return (await Universal.GET<SubredditMDRES>(url)).data;
        }

        public async Task<List<Post>> GetPosts(int amount)
        {
            string url = URL + $"&metadata=true&size={amount}&after={GetUnixTimestamp(Random(), false)}";
            return (await Universal.GET<SubredditMDRES>(url)).data;
        }

        public async Task<List<Post>> GetPosts(RedditdotnetV2.Responses.SortTime sortTime)
        {
            string url = URL + $"&metadata=true&size=100&after={GetUnixTimestamp(sortTime, false)}";
            return (await Universal.GET<SubredditMDRES>(url)).data;
        }

        public async Task<List<Post>> GetPosts(int amount, RedditdotnetV2.Responses.SortTime sortTime)
        {
            string url = URL + $"&metadata=true&size={amount}&after={GetUnixTimestamp(sortTime, false)}";
            return (await Universal.GET<SubredditMDRES>(url)).data;
        }

        public int GetCurrentUnixTimestamp()
        {
            return (int)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
        }

        public int GetUnixTimestamp(RedditdotnetV2.Responses.SortTime sortTime, bool isSummertime)
        {
            var now = DateTime.UtcNow;
            switch (sortTime)
            {
                case RedditdotnetV2.Responses.SortTime.Year:
                    now = DateTime.UtcNow.AddYears(-1);
                    break;
                case RedditdotnetV2.Responses.SortTime.Month:
                    now = DateTime.UtcNow.AddMonths(-1);
                    break;
                case RedditdotnetV2.Responses.SortTime.Week:
                    now = DateTime.UtcNow.AddDays(-7);
                    break;
                case RedditdotnetV2.Responses.SortTime.Today:
                    now = isSummertime ? DateTime.Today.AddHours(1) : DateTime.Today.AddHours(-1);
                    break;
                case RedditdotnetV2.Responses.SortTime.Hour:
                    now = DateTime.UtcNow.AddHours(-1);
                    break;
                case RedditdotnetV2.Responses.SortTime.OfAllTime:
                    now = new DateTime(2014, 1, 1);
                    break;
            }
            return (int)(now - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
        }

        public RedditdotnetV2.Responses.SortTime Random()
        {
            return (RedditdotnetV2.Responses.SortTime)(new Random().Next(0, Enum.GetNames(typeof(RedditdotnetV2.Responses.SortTime)).Length));
        }
    }
}
