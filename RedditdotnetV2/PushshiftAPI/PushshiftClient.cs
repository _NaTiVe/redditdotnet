﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;

using RedditdotnetV2.PushshiftAPI.Responses;
using RedditdotnetV2.PushshiftAPI.Endpoints;

namespace RedditdotnetV2.PushshiftAPI
{
    public class PushshiftClient
    {
        public List<SubredditMD> SubredditMDs { get; set; }
        public SubredditMD DankMemes { get; set; }
        public SubredditMD WholesomeMemes { get; set; }
        public SubredditMD Me_IRL { get; set; }

        public PushshiftClient()
        {
            DankMemes = new SubredditMD("Dank Memes MD", "dankmemes");
            WholesomeMemes = new SubredditMD("Wholesome Memes MD", "wholesomememes");
            Me_IRL = new SubredditMD("me_irl MD", "me_irl");
            SubredditMDs = new List<SubredditMD>();
        }
    }
}
