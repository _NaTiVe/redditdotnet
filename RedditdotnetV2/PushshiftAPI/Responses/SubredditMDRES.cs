﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RedditdotnetV2.PushshiftAPI.Responses;
using RedditdotnetV2.PushshiftAPI.Endpoints;

namespace RedditdotnetV2.PushshiftAPI.Responses
{
    public class SubredditMDRES
    {
        public List<Post> data { get; set; }
        public Metadata metadata { get; set; }
    }

    public class Post
    {
        public string author { get; set; }
        public string full_link { get; set; }
        public string url { get; set; }
        public int created_utc { get; set; }
        public string title { get; set; }
        public bool over_18 { get; set; }
        public bool stickied { get; set; }
        public string selftext { get; set; }
        public string permalink { get; set; }
        public string domain { get; set; }
    }
}
