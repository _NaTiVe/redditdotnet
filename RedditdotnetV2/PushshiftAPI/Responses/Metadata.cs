﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RedditdotnetV2.PushshiftAPI.Responses;
using RedditdotnetV2.PushshiftAPI.Endpoints;

namespace RedditdotnetV2.PushshiftAPI.Responses
{
    public class Metadata
    {
        public string after { get; set; }
        public string metadata { get; set; }
        public int results_returned { get; set; }
        public int total_results { get; set; }
        public List<string> subreddit { get; set; }
        public int size { get; set; }
        public Shards shards { get; set; }
        public List<Range> ranges { get; set; }
        public string sort { get; set; }
        public string sort_type { get; set; }
    }

    public class Range
    {
        public Created_UTC created_utc { get; set; }
    }

    public class Created_UTC
    {
        public int gt { get; set; }
    }

    public class Shards
    {
        public int failed { get; set; }
        public int skipped { get; set; }
        public int successful { get; set; }
        public int total { get; set; }
    }
}
