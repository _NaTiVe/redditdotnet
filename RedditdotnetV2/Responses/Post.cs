﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RedditdotnetV2.Responses
{
    public class Post
    {
        public string kind { get; set; }
        public PostData data { get; set; }
    }

    public class PostData
    {
        public string title { get; set; }
        public int ups { get; set; }
        public string domain { get; set; }
        public string link_flair_text { get; set; }
        public int score { get; set; }
        public Preview preview { get; set; }
        public string permalink { get; set; }
        public string url { get; set; }
        public bool stickied { get; set; }
        public bool is_video { get; set; }
        public string selftext { get; set; }
        public bool over_18 { get; set; }
        public bool pinned { get; set; }
        public string author { get; set; }
    }

    public class Preview
    {
        public List<Images> images { get; set; }
    }

    public class Images
    {
        public Source source { get; set; }
        public List<Resolutions> resolutions { get; set; }
        public Variants variants { get; set; }
        public string id { get; set; }
    }

    public class Source
    {
        public string url { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Resolutions
    {
        public string url { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Variants
    {
        public GIF gif { get; set; }
        public MP4 mp4 { get; set; }
    }

    public class GIF
    {
        public Source source { get; set; }
        public List<Resolutions> resolutions { get; set; }
    }

    public class MP4
    {
        public Source source { get; set; }
        public List<Resolutions> resolutions { get; set; }
    }
}
