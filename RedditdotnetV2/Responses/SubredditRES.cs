﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RedditdotnetV2.Responses
{
    public class SubredditRES
    {
        public string kind { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public string modhash { get; set; }
        public int dist { get; set; }
        public List<Post> children { get; set; }
        public string before { get; set; }
        public string after { get; set; }
    }
}
