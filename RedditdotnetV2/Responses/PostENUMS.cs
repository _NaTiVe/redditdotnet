﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedditdotnetV2.Responses
{
    public enum SortType
    {
        Frontpage,
        Hot,
        Top,
        Rising,
        Controversial,
        New
    }

    public enum SortTime
    {
        OfAllTime,
        Year,
        Month,
        Week,
        Today,
        Hour
    }
}
