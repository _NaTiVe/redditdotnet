﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

using static RedditdotnetV2.Dependencies;
using RedditdotnetV2.Responses;

namespace RedditdotnetV2.Helpers
{
    public static class SubredditEXT
    {
        public static string GetImageUrl(this Post post)
        {
            return post.data.url;
        }

        public static string GetPostUrl(this Post post)
        {
            return post.data.permalink;
        }

        public static Post GetByTitle(this List<Post> posts, string title)
        {
            return posts.Find(x => x.data.title == title);
        }

        public static Post Random(this List<Post> posts, bool includeStickied)
        {
            if (includeStickied)
                return posts[new Random().Next(posts.Count)];

            return posts.Where(x => !x.data.stickied).ToList()[new Random().Next(posts.Count)];
        }

        public static List<Post> PostsOverXUpvotes(this List<Post> posts, int upvotes)
        {
            return posts.Where(x => x.data.ups > upvotes).ToList();
        }

        public static List<Post> PostsUnderXUpvotes(this List<Post> posts, int upvotes)
        {
            return posts.Where(x => x.data.ups < upvotes).ToList();
        }
    }
}
