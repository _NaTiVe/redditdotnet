﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

using static RedditdotnetV2.Dependencies;

namespace RedditdotnetV2.Helpers
{
    public class Universal
    {
        public Uri BaseUrl { get; set; }

        public Universal(string URL)
        {
            BaseUrl = new Uri(URL);
        }

        public async Task<T> GET<T>(string url)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = BaseUrl;

                var response = await httpClient.GetAsync(url);

                if (response.StatusCode != HttpStatusCode.OK)
                    throw new HttpRequestException($"Oh oh! Something went wrong.. {response.StatusCode} | {response.ReasonPhrase}");

                string json = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<T>(json);
            }
        }
    }
}
